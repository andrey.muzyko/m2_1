﻿using System;
using System.Collections.Generic;

namespace Students
{
    class Student
    {
        public string FullName;
        public string Email;

        public Student(string email)
        {
            int dotPosition = email.IndexOf(".");
            int atPosition = email.IndexOf("@", dotPosition);
            FullName = email.Substring(0, 1).ToUpper() + email.Substring(1, dotPosition - 1) + " " + email.Substring(dotPosition + 1, 1).ToUpper() + email.Substring(dotPosition + 2, atPosition - dotPosition - 2);
            Email = email;
        }

        public Student(string firstName, string surname)
        {
            FullName = firstName + " " + surname;
            Email = firstName.ToLower() + "." + surname.ToLower() + "@epam.com";
        }

        public override bool Equals(object obj)
        {
            if (obj.GetType() != this.GetType()) return false;

            Student student = (Student)obj;
            return (this.FullName == student.FullName);
        }

        public override int GetHashCode()
        {
            return FullName.GetHashCode();
        }
    }

    class Program
    {
        static HashSet<string> FunctionRandNAdd(string[] subjects, int n)
        {
            HashSet<string> hs = new();
            List<string> subjectsCopy = new();
            int len = subjects.Length;
            for (int i = 0; i < len; i++)
            {
                subjectsCopy.Add(subjects[i]);
            }
            for (int i = 0; i < n; i++)
            {
                Random rnd = new ();
                int randIndex = rnd.Next(0, len);
                hs.Add(subjectsCopy[randIndex]);
                subjectsCopy.RemoveAt(randIndex);
                len--;
            }
            return hs;
        }

        static void Main(string[] args)
        {
            var student1c1 = new Student("vasya.pupkin@epam.com");
            var student2c1 = new Student("dmitriy.fedorov@epam.com");
            var student3c1 = new Student("nikita.semenov@epam.com");
            var student1c2 = new Student("Vasya", "Pupkin");
            var student2c2 = new Student("Dmitriy", "Fedorov");
            var student3c2 = new Student("Nikita", "Semenov");

            string[] subjects = new string[] { "Maths", "Literature", "Chemistry", "English", "Phisics", "History" };
            int n = 3; // количество случайных элементов в множестве

            Dictionary<Student, HashSet<string>> studentSubjectDict = new();
            studentSubjectDict[student1c1] = FunctionRandNAdd(subjects, n);
            studentSubjectDict[student2c1] = FunctionRandNAdd(subjects, n);
            studentSubjectDict[student3c1] = FunctionRandNAdd(subjects, n);
            studentSubjectDict[student1c2] = FunctionRandNAdd(subjects, n);
            studentSubjectDict[student2c2] = FunctionRandNAdd(subjects, n);
            studentSubjectDict[student3c2] = FunctionRandNAdd(subjects, n);

            foreach (KeyValuePair<Students.Student, HashSet<string>> hh in studentSubjectDict)
            {
                Console.WriteLine(); 
                Console.Write(hh.Key.FullName + " : ");
                HashSet<string> val = hh.Value;
                foreach (string str in val)
                    Console.Write(str + " ");
            }
            Console.WriteLine();
        }
    }
}
